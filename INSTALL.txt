********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: sitebrowser module
Author: Kristy Frey <Kristy at AkronMedia.com>
********************************************************************
INSTALLATION:

1. Place the entire sitebrowser directory into your Drupal modules/
   directory.

2. Enable this module by navigating to:

     administer > modules

Installation is finished. To have your visitors use this style of site browsing, adjust your home page to start at http://example.com/index.php?q=sitebrowser and adjust your template to include the global and local navigation blocks.  This module requires user permissions, but it is designed for use by all so it is recommended that you give anonymous and authenticated roles view permission.